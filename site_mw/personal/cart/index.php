<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>
<?php /*<div class="pmenu">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"personal",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "personal",
		"SEPARATORS_PLACE" => array(0=>"2",1=>"5",2=>"",),
		"USE_EXT" => "N"
	)
);?>
</div>

 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => "",
		"SITE_ID" => "-",
		"START_FROM" => "0"
	)
);?>*/ ?>
    <div class="pcontent">
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket",
            "gopro",
            Array(
                "ACTION_VARIABLE" => "action",
                "AJAX_MODE" => "Y",
                "COLUMNS_LIST" => array(0=>"NAME",1=>"DISCOUNT",2=>"DELETE",3=>"DELAY",4=>"TYPE",5=>"PRICE",6=>"QUANTITY",7=>"SUM",8=>"PROPERTY_CML2_ARTICLE",9=>"PROPERTY_YEAR",10=>"PROPERTY_OS",11=>"PROPERTY_WEIGHT",),
                "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                "HIDE_COUPON" => "N",
                "OFFERS_PROPS" => array(0=>"COLOR_DIRECTORY",1=>"COLOR2_DIRECTORY",2=>"STORAGE",),
                "PATH_TO_ORDER" => "/site_mw/personal/order/make/",
                "PRICE_VAT_SHOW_VALUE" => "N",
                "PROP_ARTICLE" => "CML2_ARTICLE",
                "PROP_SKU_ARTICLE" => "CML2_ARTICLE",
                "QUANTITY_FLOAT" => "N",
                "SET_TITLE" => "N",
                "USE_PREPAYMENT" => "N"
            )
        );?> <?php
        $productId = $productCode = 0;
        if (CModule::IncludeModule('sale'))
        {
            $basketItem = CSaleBasket::GetList(
                array(),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ),
                array()
            )->Fetch();
            $productId = $basketItem['ID'];
            $productCode = $basketItem['CODE'];
        }
        ?>
    </div>
<? require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>