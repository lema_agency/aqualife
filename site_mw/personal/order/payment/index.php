<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?><div class="pmenu">
</div>
    <div class="pcontent">
        <?$APPLICATION->IncludeComponent("bitrix:sale.order.ajax", "gopro_payment", Array(
	"ALLOW_AUTO_REGISTER" => "N",	// Оформлять заказ с автоматической регистрацией пользователя
		"ALLOW_NEW_PROFILE" => "Y",	// Разрешить множество профилей покупателей
		"CODE_PHONES" => "",	// Символьный код свойства, являющегося телефоном
		"COMPONENT_TEMPLATE" => "gopro",
		"COUNT_DELIVERY_TAX" => "N",	// Рассчитывать налог для доставки
		"DELIVERY_NO_AJAX" => "N",	// Рассчитывать стоимость доставки сразу
		"DELIVERY_NO_SESSION" => "N",	// Проверять сессию при оформлении заказа
		"DELIVERY_TO_PAYSYSTEM" => "d2p",	// Последовательность оформления
		"DISABLE_BASKET_REDIRECT" => "N",	// Оставаться на странице, если корзина пуста
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",	// Позволять оплачивать с внутреннего счета только в полном объеме
		"PATH_TO_AUTH" => "/site_mw/auth/",	// Страница авторизации
		"PATH_TO_BASKET" => "/site_mw/personal/cart/",	// Страница корзины
		"PATH_TO_PAYMENT" => "/site_mw/personal/order/payment/",	// Страница подключения платежной системы
		"PATH_TO_PERSONAL" => "/site_mw/personal/",	// Страница персонального раздела
		"PAY_FROM_ACCOUNT" => "Y",	// Позволять оплачивать с внутреннего счета
		"PRODUCT_COLUMNS" => "",	// Дополнительные колонки таблицы товаров заказа
		"PROP_1" => "",	// Не показывать свойства для типа плательщика "Физическое лицо" (mw)
		"PROP_2" => "",	// Не показывать свойства для типа плательщика "Юридическое лицо" (mw)
		"SEND_NEW_USER_NOTIFY" => "N",	// Отправлять пользователю письмо, что он зарегистрирован на сайте
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",	// Отображать названия платежных систем
		"SHOW_STORES_IMAGES" => "N",	// Показывать изображения складов в окне выбора пункта выдачи
		"TEMPLATE_LOCATION" => "popup",	// Шаблон местоположения
		"USE_PREPAYMENT" => "N",	// Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
	),
	false
);?>
    </div>
    <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>