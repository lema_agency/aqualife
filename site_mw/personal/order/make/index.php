<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?><div class="pmenu">
</div>
<div class="pcontent">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax",
	"gopro",
	Array(
		"ALLOW_AUTO_REGISTER" => "N",
		"ALLOW_NEW_PROFILE" => "Y",
		"CODE_PHONES" => array(),
		"COMPONENT_TEMPLATE" => "gopro",
		"COUNT_DELIVERY_TAX" => "N",
		"DELIVERY_NO_AJAX" => "N",
		"DELIVERY_NO_SESSION" => "N",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"DISABLE_BASKET_REDIRECT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"PATH_TO_AUTH" => "/site_mw/auth/",
		"PATH_TO_BASKET" => "/site_mw/personal/cart/",
		"PATH_TO_PAYMENT" => "/site_mw/personal/order/payment/",
		"PATH_TO_PERSONAL" => "/site_mw/personal/",
		"PAY_FROM_ACCOUNT" => "Y",
		"PRODUCT_COLUMNS" => array(),
		"PROP_1" => array(),
		"PROP_2" => array(),
		"SEND_NEW_USER_NOTIFY" => "N",
		"SET_TITLE" => "Y",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "N",
		"TEMPLATE_LOCATION" => "popup",
		"USE_PREPAYMENT" => "N"
	)
);?>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>