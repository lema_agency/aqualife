<?php

if ($_SERVER['SCRIPT_NAME']=="/bitrix/admin/fileman_file_edit.php")
{
   AddEventHandler("main", "OnEpilog", "InitPHPHighlight");
   function InitPHPHighlight()
   {
      global $APPLICATION;
      $path = '/scripts/codeMirror';
          $string='<link rel="stylesheet" href="' . $path . '/codemirror.css">
      <script src="' . $path . '/codemirror.js"></script>
      <script src="' . $path . '/xml.js"></script>
      <!--<link rel="stylesheet" href="' . $path . '/javascript.css">
      <link rel="stylesheet" href="' . $path . '/clike.css">-->
      <script src="' . $path . '/javascript.js"></script>      
      <script src="' . $path . '/php.js"></script>      
      <script src="' . $path . '/clike.js"></script>      
      <style>
   
      .CodeMirror {
        overflow: auto;
        background:white;
        height: 500px;
        width: 100%;
        max-width: 1400px;
        line-height: 1em;
        font-family: inherit;
      }
      .CodeMirror pre{

        font-size:14px;
        line-height: 1.3em;
      }
      .activeline {background: #E6E6FA !important;}
      </style>';
   $init='<script type=\'text/javascript\'>
            BX.ready(function(){
            var nl=document.getElementsByTagName("textarea");
            var editor = CodeMirror.fromTextArea(nl[0], {
                     lineNumbers: true,
                     matchBrackets: true,
                     mode: "application/x-httpd-php",
                     indentUnit: 8,
                     indentWithTabs: true,
                     enterMode: "keep",
                     tabMode: "classic",
                     onCursorActivity: function() {
                     editor.setLineClass(hlLine, null);
                     hlLine = editor.setLineClass(editor.getCursor().line, "activeline");
                    }
                    });
                    var hlLine = editor.setLineClass(0, "activeline");
                    });
            </script>';
      
      $APPLICATION->AddHeadString($string);      
      $APPLICATION->AddHeadString($init);
   }
}
?>
