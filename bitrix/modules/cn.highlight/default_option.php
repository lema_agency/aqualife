<?
$cn_highlight_default_option = array(
	"language" => "en",
	"syntax" => "php",
	"start_highlight" => "Y",
	"min_width" => 400,
	"min_height" => 100,
	"allow_resize" => "both",
	"allow_toggle" => "Y",
	"plugins" => "",
	"browsers" => "known",
	"display" => "later",
	"font_size" => 10,
	"font_family" => "monospace",
	"cursor_position" => "begin",
	"gecko_spellcheck" => "N",
	"max_undo" => 20,
	"fullscreen" => "N",
	"is_editable" => "Y",
	"word_wrap" => "N",
	"replace_tab_by_spaces" => "N",
	"debug" => "N",
);
?>
