<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-18);
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

Class cn_highlight extends CModule
{
	var $MODULE_ID = "cn.highlight";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	function cn_highlight()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = CN_HIGHLIGHT_VERSION;
			$this->MODULE_VERSION_DATE = CN_HIGHLIGHT_VERSION_DATE;
		}
		$this->PARTNER_NAME = 'Info-Expert';
		$this->PARTNER_URI = 'http://info-expert.ru/';
		$this->MODULE_NAME = GetMessage("CN_HIGHLIGHT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("CN_HIGHLIGHT_MODULE_DESC");
	}

	function InstallDB($arParams = array())
	{
		RegisterModule("cn.highlight");
		RegisterModuleDependences("main", "OnPageStart", "cn.highlight", "CNHighlight", "AddJSHighlight", 100);
		RegisterModuleDependences("main", "OnEndBufferContent", "cn.highlight", "CNHighlight", "AddCodeOnEndBufferContent", 100);

		return true;
	}

	function UnInstallDB($arParams = array())
	{
		UnRegisterModuleDependences("main", "OnPageStart", "cn.highlight", "CNHighlight", "AddJSHighlight");
		UnRegisterModuleDependences("main", "OnEndBufferContent", "cn.highlight", "CNHighlight", "AddCodeOnEndBufferContent");
		UnRegisterModule("cn.highlight");

		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/cn.highlight/install/js', $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);

		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFilesEx("/bitrix/js/cn.highlight");

		return true;
	}

	function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallDB();
		$this->InstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("CN_HIGHLIGHT_INSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/cn.highlight/install/step.php");
	}

	function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallDB();
		$this->UnInstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("CN_HIGHLIGHT_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/cn.highlight/install/unstep.php");
	}
}
?>