<?
class CNHighlight
{
	function AddJSHighlight()
	{
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/fileman_file_edit.php")
		{
			$GLOBALS["APPLICATION"]->AddHeadScript("/bitrix/js/cn.highlight/edit_area_compressor.php?plugins");
			$gecko = (COption::GetOptionString("cn.highlight", "gecko_spellcheck", "N") == "N")? "false" : "true";
			$fullscreen = (COption::GetOptionString("cn.highlight", "fullscreen", "N") == "N")? "false" : "true";
			$start_highlight = (COption::GetOptionString("cn.highlight", "start_highlight", "Y") == "Y")? "true" : "false";
			$allow_toggle = (COption::GetOptionString("cn.highlight", "allow_toggle", "Y") == "Y")? "true" : "false";
			$is_editable = (COption::GetOptionString("cn.highlight", "is_editable", "Y") == "Y")? "true" : "false";
			$word_wrap = (COption::GetOptionString("cn.highlight", "word_wrap", "N") == "N")? "false" : "true";
			$replace_tab_by_spaces = (COption::GetOptionString("cn.highlight", "replace_tab_by_spaces", "N") == "N")? "false": COption::GetOptionString("cn.highlight", "replace_tab_by_spaces", "N");
			$debug = (COption::GetOptionString("cn.highlight", "debug", "N") == "N")? "false" : "true";
			$GLOBALS["APPLICATION"]->AddHeadString('<script language="Javascript" type="text/javascript">
			editAreaLoader.init({
			id: "filesrc",
			language: "'.COption::GetOptionString("cn.highlight", "language", "en").'",
			start_highlight: '.$start_highlight.',
			syntax: "'.COption::GetOptionString("cn.highlight", "syntax", "php").'",
			min_width: '.COption::GetOptionString("cn.highlight", "min_width", "400").',
			min_height: '.COption::GetOptionString("cn.highlight", "min_height", "100").',
			allow_resize: "'.COption::GetOptionString("cn.highlight", "allow_resize", "both").'",
			allow_toggle: '.$allow_toggle.',
			plugins: "'.COption::GetOptionString("cn.highlight", "plugins", "").'",
			browsers: "'.COption::GetOptionString("cn.highlight", "browsers", "known").'",
			display: "'.COption::GetOptionString("cn.highlight", "display", "later").'",
			font_size: '.COption::GetOptionString("cn.highlight", "font_size", "10").',
			font_family: "'.COption::GetOptionString("cn.highlight", "font_family", "monospace").'",
			cursor_position: "'.COption::GetOptionString("cn.highlight", "cursor_position", "begin").'",
			gecko_spellcheck: '.$gecko.',
			max_undo: '.COption::GetOptionString("cn.highlight", "max_undo", "20").',
			fullscreen: '.$fullscreen.',
			is_editable: '.$is_editable.',
			word_wrap: '.$word_wrap.',
			replace_tab_by_spaces: '.$replace_tab_by_spaces.',
			debug: '.$debug.',
			toolbar: "search, go_to_line, fullscreen, |, undo, redo, |, select_font,|, change_smooth_selection, highlight, reset_highlight, word_wrap, |, help,| ,syntax_selection"
			});</script>');
		}
	}

	function AddCodeOnEndBufferContent(&$content)
	{
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/fileman_file_edit.php")
		{
			$content = str_replace('name="filesrc"', 'name="filesrc" id="filesrc"', $content);
		}
	}
}
?>