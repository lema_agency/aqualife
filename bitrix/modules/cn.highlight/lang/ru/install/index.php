<?
$MESS ['CN_HIGHLIGHT_MODULE_NAME'] = "Подсветка синтаксиса";
$MESS ['CN_HIGHLIGHT_MODULE_DESC'] = "Модуль подсвечивающий синтаксис кода страницы сайта при редактировании. <a href='http://codenails.com'>CodeNails</a>";
$MESS ['CN_HIGHLIGHT_INSTALL_TITLE'] = "Установка модуля подсветки синтаксиса";
$MESS ['CN_HIGHLIGHT_UNINSTALL_TITLE'] = "Удаление модуля подсветки синтаксиса";
?>