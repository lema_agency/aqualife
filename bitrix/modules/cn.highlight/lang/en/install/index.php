<?
$MESS ['CN_HIGHLIGHT_MODULE_NAME'] = "Syntax Highlighting";
$MESS ['CN_HIGHLIGHT_MODULE_DESC'] = "The module has syntax highlighting for code pages while editing. <a href='http://codenails.com'>CodeNails</a>";
$MESS ['CN_HIGHLIGHT_INSTALL_TITLE'] = "Installation of the module syntax";
$MESS ['CN_HIGHLIGHT_UNINSTALL_TITLE'] = "Removing the module syntax";
?>