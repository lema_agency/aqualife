<?
if(!$USER->IsAdmin())
	return;

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);



$arAllOptions = Array(

	Array("language", GetMessage("CN_HIGHLIGHT_LANGUAGE"), "en", Array("selectbox", Array("REFERENCE" => Array(GetMessage("CN_HIGHLIGHT_LANGUAGE_EN"), GetMessage("CN_HIGHLIGHT_LANGUAGE_BG"), GetMessage("CN_HIGHLIGHT_LANGUAGE_CS"), GetMessage("CN_HIGHLIGHT_LANGUAGE_DE"), GetMessage("CN_HIGHLIGHT_LANGUAGE_DK"), GetMessage("CN_HIGHLIGHT_LANGUAGE_EO"), GetMessage("CN_HIGHLIGHT_LANGUAGE_ES"), GetMessage("CN_HIGHLIGHT_LANGUAGE_FI"), GetMessage("CN_HIGHLIGHT_LANGUAGE_FR"), GetMessage("CN_HIGHLIGHT_LANGUAGE_HR"), GetMessage("CN_HIGHLIGHT_LANGUAGE_IT"), GetMessage("CN_HIGHLIGHT_LANGUAGE_JA"), GetMessage("CN_HIGHLIGHT_LANGUAGE_MK"), GetMessage("CN_HIGHLIGHT_LANGUAGE_NL"), GetMessage("CN_HIGHLIGHT_LANGUAGE_PL"), GetMessage("CN_HIGHLIGHT_LANGUAGE_PT"), GetMessage("CN_HIGHLIGHT_LANGUAGE_RU"), GetMessage("CN_HIGHLIGHT_LANGUAGE_SK"), GetMessage("CN_HIGHLIGHT_LANGUAGE_ZH")), "REFERENCE_ID" => Array("en" , "bg", "cs", "de", "dk", "eo", "es", "fi", "fr", "hr", "it", "ja", "mk", "nl", "pl", "pt", "ru", "sk", "zh")))),
	Array("syntax", GetMessage("CN_HIGHLIGHT_SYNTAX"), "php", Array("selectbox", Array("REFERENCE" => Array(GetMessage("CN_HIGHLIGHT_SYNTAX_BASIC"), GetMessage("CN_HIGHLIGHT_SYNTAX_BRAINFUCK"), GetMessage("CN_HIGHLIGHT_SYNTAX_C"), GetMessage("CN_HIGHLIGHT_SYNTAX_COLDFUSION"), GetMessage("CN_HIGHLIGHT_SYNTAX_CPP"), GetMessage("CN_HIGHLIGHT_SYNTAX_CSS"), GetMessage("CN_HIGHLIGHT_SYNTAX_HTML"), GetMessage("CN_HIGHLIGHT_SYNTAX_JAVA"), GetMessage("CN_HIGHLIGHT_SYNTAX_JS"), GetMessage("CN_HIGHLIGHT_SYNTAX_PAS"), GetMessage("CN_HIGHLIGHT_SYNTAX_PERL"), GetMessage("CN_HIGHLIGHT_SYNTAX_PHP"), GetMessage("CN_HIGHLIGHT_SYNTAX_PYTHON"), GetMessage("CN_HIGHLIGHT_SYNTAX_ROBOTSTXT"), GetMessage("CN_HIGHLIGHT_SYNTAX_RUBY"), GetMessage("CN_HIGHLIGHT_SYNTAX_SQL"), GetMessage("CN_HIGHLIGHT_SYNTAX_TSQL"), GetMessage("CN_HIGHLIGHT_SYNTAX_VB"), GetMessage("CN_HIGHLIGHT_SYNTAX_XML")), "REFERENCE_ID" => Array("basic", "brainfuck", "c", "coldfusion", "cpp", "css", "html", "java", "js", "pas", "perl", "php", "python", "robotstxt", "ruby", "sql", "tsql", "vb", "xml")))),
	Array("start_highlight", GetMessage("CN_HIGHLIGHT_START_HIGHLIGHT"), "Y", Array("checkbox", "N")),
// 	Array("is_multi_files", GetMessage("CN_HIGHLIGHT_IS_MULTI_FILES"), "N", Array("checkbox", "Y")),
	Array("min_width", GetMessage("CN_HIGHLIGHT_MIN_WIDTH"), "400", Array("text", 15)),
	Array("min_height", GetMessage("CN_HIGHLIGHT_MIN_HEIGHT"), "100", Array("text", 15)),
	Array("allow_resize", GetMessage("CN_HIGHLIGHT_ALLOW_RESIZE"), "both", Array("selectbox", Array("REFERENCE" => Array(GetMessage("CN_HIGHLIGHT_ALLOW_RESIZE_NO"), GetMessage("CN_HIGHLIGHT_ALLOW_RESIZE_BOTH"), GetMessage("CN_HIGHLIGHT_ALLOW_RESIZE_X"), GetMessage("CN_HIGHLIGHT_ALLOW_RESIZE_Y")), "REFERENCE_ID" => Array("no", "both", "x", "y")))),
	Array("allow_toggle", GetMessage("CN_HIGHLIGHT_ALLOW_TOGGLE"), "Y", Array("checkbox", "N")),
	Array("plugins", GetMessage("CN_HIGHLIGHT_PLUGINS"), "", Array("text", 15)),
	Array("browsers", GetMessage("CN_HIGHLIGHT_BROWSERS"), "known", Array("selectbox", Array("REFERENCE" => Array(GetMessage("CN_HIGHLIGHT_BROWSERS_ALL"), GetMessage("CN_HIGHLIGHT_BROWSERS_KNOWN")), "REFERENCE_ID" => Array("all", "known")))),
	Array("display", GetMessage("CN_HIGHLIGHT_DISPLAY"), "later", Array("selectbox", Array("REFERENCE" => Array(GetMessage("CN_HIGHLIGHT_DISPLAY_ONLOAD"), GetMessage("CN_HIGHLIGHT_DISPLAY_LATER")), "REFERENCE_ID" => Array("onload", "later")))),
// 	Array("toolbar", GetMessage("CN_HIGHLIGHT_TOOLBAR"), "50", Array("text", 15)),
// 	Array("begin_toolbar", GetMessage("CN_HIGHLIGHT_BEGIN_TOOLBAR"), "50", Array("text", 15)),
// 	Array("end_toolbar", GetMessage("CN_HIGHLIGHT_END_TOOLBAR"), "50", Array("text", 15)),
	Array("font_size", GetMessage("CN_HIGHLIGHT_FONT_SIZE"), "10", Array("text", 15)),
	Array("font_family", GetMessage("CN_HIGHLIGHT_FONT_FAMILY"), "monospace", Array("text", 15)),
	Array("cursor_position", GetMessage("CN_HIGHLIGHT_CURSOR_POSITION"), "begin", Array("selectbox", Array("REFERENCE" => Array(GetMessage("CN_HIGHLIGHT_CURSOR_POSITION_BEGIN"), GetMessage("CN_HIGHLIGHT_CURSOR_POSITION_AUTO")), "REFERENCE_ID" => Array("begin", "auto")))),
	Array("gecko_spellcheck", GetMessage("CN_HIGHLIGHT_GECKO_SPELLCHECK"), "N", Array("checkbox", "Y")),
	Array("max_undo", GetMessage("CN_HIGHLIGHT_MAX_UNDO"), "20", Array("text", 5)),
	Array("fullscreen", GetMessage("CN_HIGHLIGHT_FULLSCREEN"), "N", Array("checkbox", "Y")),
	Array("is_editable", GetMessage("CN_HIGHLIGHT_IS_EDITABLE"), "Y", Array("checkbox", "Y")),
	Array("word_wrap", GetMessage("CN_HIGHLIGHT_WORD_WRAP"), "N", Array("checkbox", "Y")),
	Array("replace_tab_by_spaces", GetMessage("CN_HIGHLIGHT_REPLACE_TAB_BY_SPACES"), "N", Array("selectbox", Array("REFERENCE" => Array(GetMessage("CN_HIGHLIGHT_REPLACE_TAB_BY_SPACES_FALSE"), GetMessage("CN_HIGHLIGHT_REPLACE_TAB_BY_SPACES_2"), GetMessage("CN_HIGHLIGHT_REPLACE_TAB_BY_SPACES_4"), GetMessage("CN_HIGHLIGHT_REPLACE_TAB_BY_SPACES_6"), GetMessage("CN_HIGHLIGHT_REPLACE_TAB_BY_SPACES_8")), "REFERENCE_ID" => Array("N", 2, 4, 6, 8)))),
	Array("debug", GetMessage("CN_HIGHLIGHT_DEBUG"), "N", Array("checkbox", "Y")),
);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "main_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid())
{
	if(strlen($RestoreDefaults)>0)
	{
		COption::RemoveOption("cn.highlight");
	}
	else
	{
		foreach($arAllOptions as $arOption)
		{
			$name=$arOption[0];
			$val=$_REQUEST[$name];
			if($arOption[3][0]=="checkbox" && $val!="Y")
				$val="N";
			COption::SetOptionString("cn.highlight", $name, $val, $arOption[1]);
		}
	}
	if(strlen($Update)>0 && strlen($_REQUEST["back_url_settings"])>0)
		LocalRedirect($_REQUEST["back_url_settings"]);
	else
		LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
}


$tabControl->Begin();
?>
<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?echo LANGUAGE_ID?>">
<?$tabControl->BeginNextTab();?>
	<?
	foreach($arAllOptions as $arOption):
		$val = COption::GetOptionString("cn.highlight", $arOption[0], $arOption[2]);
		$type = $arOption[3];
	?>
	<tr>
		<td valign="top" width="50%"><?
			if($type[0]=="checkbox")
				echo "<label for=\"".htmlspecialchars($arOption[0])."\">".$arOption[1]."</label>";
			else
				echo $arOption[1];?>:</td>
		<td valign="top" width="50%">
			<?if($type[0]=="checkbox"):?>
				<input type="checkbox" id="<?echo htmlspecialchars($arOption[0])?>" name="<?echo htmlspecialchars($arOption[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
			<?elseif($type[0]=="text"):?>
				<input type="text" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialchars($val)?>" name="<?echo htmlspecialchars($arOption[0])?>">
			<?elseif($type[0]=="textarea"):?>
				<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialchars($arOption[0])?>"><?echo htmlspecialchars($val)?></textarea>
			<?elseif($type[0]=="selectbox"):?>
				<?echo SelectBoxFromArray($arOption[0], $arOption[3][1], $val)?>
			<?endif?>
		</td>
	</tr>
	<?endforeach?>
<?$tabControl->Buttons();?>
	<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>">
	<input type="submit" name="Apply" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
	<?if(strlen($_REQUEST["back_url_settings"])>0):?>
		<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?echo htmlspecialchars(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
		<input type="hidden" name="back_url_settings" value="<?=htmlspecialchars($_REQUEST["back_url_settings"])?>">
	<?endif?>
	<input type="submit" name="RestoreDefaults" title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
	<?=bitrix_sessid_post();?>
<?$tabControl->End();?>
</form>
