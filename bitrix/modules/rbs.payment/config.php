<?
/**
 * Комментарии при установке и при настройке
 */
$mess["module_name"] = "Прием платежей через РосЕвроБанк";
$mess["module_description"] = "РосЕвроБанк - http://www.rosevrobank.ru/";
$mess["partner_name"] = "РосЕвроБанк";
$mess["partner_uri"] = "http://www.rosevrobank.ru/";

/**
 * URL API
 */
define('PROD_URL', 'https://3dsec.paymentgate.ru/ipay/rest/'); // Продакшн/Бой
define('TEST_URL', 'https://test.paymentgate.ru/rebpayment/rest/'); // Тест

/**
 * Если utf-8, то true. Если cp1251 - false (специфично для Bitrix)
 */
define(ENCODING, true);

/**
 * Версия плагина
 */
define(VERSION, '2.1');
define(VERSION_DATE, '2015-06-17 18:00:00');
