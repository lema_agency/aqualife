<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?><form name="<?=$arResult['FILTER_NAME'].'_form'?>" action="<?=$arResult['FORM_ACTION']?>" method="get" class="smartfilter" onsubmit="return RSGoPro_FilterOnSubmitForm();"><?
    foreach($arResult['HIDDEN'] as $arItem) {
        ?><input <?
            ?>type="hidden" <?
            ?>name="<?=$arItem['CONTROL_NAME']?>" <?
            ?>id="<?=$arItem['CONTROL_ID']?>" <?
            ?>value="<?=$arItem['HTML_VALUE']?>" <?
        ?>/> <?
    }
    ?><div class="around_filtren"><?
        ?><div class="filtren clearfix<?
            if($arParams['FILTER_FIXED']=='Y'):?> filterfixed<?endif;?><?
            if($arParams['FILTER_USE_AJAX']=='Y'):?> ajaxfilter<?endif;?><?
            echo ' '.$arParams['FILTER_DISABLED_PIC_EFFECT'];
            ?>"><?
            ?><div class="title"><?
                ?><a class="shhi" href="#"><span class="show"><?=GetMessage('CT_BCSF_FILTER_TITLE_SHOW')?></span><span class="hide"><?=GetMessage('CT_BCSF_FILTER_TITLE_HIDE')?></span></a><?
                if($arParams['USE_COMPARE']=='Y') {
                    ?><span class="filtercompare"><?=GetMessage('FILTER_COMPARE')?>: <a href="#"></a></span><?
                }
            ?></div><?
            ?><div class="body"><?
                ?><ul class="clearfix"><?

                // prices
                /*
                foreach($arResult['ITEMS'] as $key=>$arItem) {
                    $key = $arItem["ENCODED_ID"];
                    if(isset($arItem["PRICE"])) {
                        /*if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"]) {
                            continue;
                        }*/ /*
                        $precision = 2;
                        if (Bitrix\Main\Loader::includeModule("currency"))
                        {
                            $res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
                            $precision = $res['DECIMALS'];
                        }
                        ?><li class="lvl1<?if($IS_SCROLABLE):?> scrolable<?endif;?><?if($IS_SEARCHABLE):?> searcheble<?endif;?>" data-propid="<?=$arItem['ID']?>" data-propcode="<?=$arItem['CODE']?>"><?
                            ?><a href="#" class="showchild"><i class="icon pngicons"></i><?=$arItem['NAME']?></a><?
                            ?><ul class="property number"><?
                                ?><div class="inputs"><?
                                    ?><span class="from"><?=GetMessage('CT_BCSF_FILTER_FROM')?></span><?
                                    ?><input
                                        class="min-price min"
                                        type="text"
                                        name="<?=$arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                        id="<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                        value="<?=$arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                        size="9"
                                        onkeyup="smartFilter.keyup(this)"
                                    /><?
                                    ?><span class="separator"></span><?
                                    ?><span class="to"><?=GetMessage("CT_BCSF_FILTER_TO")?></span><?
                                    ?><input
                                        class="max-price max"
                                        type="text"
                                        name="<?=$arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                        id="<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                        value="<?=$arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                        size="9"
                                        onkeyup="smartFilter.keyup(this)"
                                    /><?
                                ?></div><?
                                ?><div class="aroundslider"><?
                                    ?><div style="clear: both;"></div><?
                                    ?><div class="bx_ui_slider_track" id="drag_track_<?=$key?>"><?
                                        $precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
                                        $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
                                        $price1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
                                        $price2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
                                        $price3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
                                        $price4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
                                        $price5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                                        ?><div class="bx_ui_slider_part p1"><span><?=$price1?></span></div><?
                                        ?><div class="bx_ui_slider_part p2"><span><?=$price2?></span></div><?
                                        ?><div class="bx_ui_slider_part p3"><span><?=$price3?></span></div><?
                                        ?><div class="bx_ui_slider_part p4"><span><?=$price4?></span></div><?
                                        ?><div class="bx_ui_slider_part p5"><span><?=$price5?></span></div><?
                                        ?><div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div><?
                                        ?><div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div><?
                                        ?><div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div><?
                                        ?><div class="bx_ui_slider_range"     id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;"><?
                                            ?><a class="bx_ui_slider_handle left pngicons"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a><?
                                            ?><a class="bx_ui_slider_handle right pngicons" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a><?
                                        ?></div><?
                                    ?></div><?
                                ?></div><?
                                if (Bitrix\Main\Loader::includeModule("currency"))
                                {
                                    $res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
                                    $precision = $res['DECIMALS'];
                                }
                                $arJsParams = array(
                                    "leftSlider" => 'left_slider_'.$key,
                                    "rightSlider" => 'right_slider_'.$key,
                                    "tracker" => "drag_tracker_".$key,
                                    "trackerWrap" => "drag_track_".$key,
                                    "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                                    "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                                    "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                                    "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                                    "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                    "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                    "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
                                    "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                                    "precision" => $precision,
                                    "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                                    "colorAvailableActive" => 'colorAvailableActive_'.$key,
                                    "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                                );
                            ?></ul><?
                            ?><script type="text/javascript">
                                BX.ready(function(){
                                    window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                                });
                            </script><?
                        ?></li><?
                    }
                }
                  */
                // simple
                foreach($arResult['ITEMS'] as $key=>$arItem) {
                    if( empty($arItem['VALUES']) || isset($arItem['PRICE']) ) {
                        continue;
                    }

                    if(
                        $arItem["DISPLAY_TYPE"] == "A"
                        && (
                            !$arItem["VALUES"]["MIN"]["VALUE"]
                            || !$arItem["VALUES"]["MAX"]["VALUE"]
                            || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"]
                        )
                    ) {
                        continue;
                    }
                    // scroll
                    if(in_array($arItem['CODE'],$arParams['FILTER_PROP_SCROLL']) || in_array($arItem['CODE'],$arParams['FILTER_SKU_PROP_SCROLL'])) {
                        $IS_SCROLABLE = true;
                    } else {
                        $IS_SCROLABLE = false;
                    }
                    // search
                    if(in_array($arItem['CODE'],$arParams['FILTER_PROP_SEARCH']) || in_array($arItem['CODE'],$arParams['FILTER_SKU_PROP_SEARCH'])) {
                        $IS_SEARCHABLE = true;
                    } else {
                        $IS_SEARCHABLE = false;
                    }
                    ?><li style="list-style-type: none;" class="lvl1<?if($IS_SCROLABLE):?> scrolable<?endif;?><?if($IS_SEARCHABLE):?> searcheble<?endif;?><?if($arItem["DISPLAY_EXPANDED"]!="Y"):?> closed<?endif?>" data-propid="<?=$arItem['ID']?>" data-propcode="<?=$arItem['CODE']?>"><?
                        ?><a href="#" class="showchild"><i class="icon pngicons"></i><?=$arItem['NAME']?><?if($arItem['FILTER_HINT']<>''):?><span class="hint">?<div><?=$arItem['FILTER_HINT']?></div></span><?endif;?></a><?
                        $arCur = current($arItem["VALUES"]);
                        switch ($arItem["DISPLAY_TYPE"]) {

                            default://CHECKBOXES
                                ?><ul class="property"><?
                                    // search
                                    if($IS_SEARCHABLE && !$IS_COLOR) {
                                        ?><div class="around_f_search"><input type="text" class="f_search" name="f_search" id="f_search" value="" placeholder="<?=GetMessage('FILTR_SEARHC')?>"></div><?
                                    }
                                    // scroll
                                    if($IS_SCROLABLE && count($arItem['VALUES'])>$arParams['FILTER_PROP_SCROLL_CNT']) {
                                        $IS_SCROLABLE = true;
                                    } else {
                                        $IS_SCROLABLE = false;
                                    }
                                    if($IS_SCROLABLE) {
                                        ?><div class="f_jscrollpane" id="f_scroll_<?=$arItem['ID']?>"><?
                                    }
                                    foreach ($arItem["VALUES"] as $val => $ar) {
                                        $class = "";
                                        if($ar["CHECKED"]) {
                                            $class.= " active";
                                        }
                                        if($ar["DISABLED"]) {
                                            if($class != ' active') {
                                                $class.= " disabled";
                                            }
                                        }
                                        ?><li class="lvl2" style="list-style-type: none;"><?
                                            ?><div class="<?=$class?>"><?
                                                ?><input <?
                                                    ?>type="checkbox" <?
                                                    ?>name="<?=$ar["CONTROL_NAME"]?>" <?
                                                    ?>id="<?=$ar["CONTROL_ID"]?>" <?
                                                    ?>value="<?=$ar["HTML_VALUE"]?>" <?
                                                    ?><? echo $ar["CHECKED"]? 'checked="checked"': '' ?> <?
                                                ?>/><?
                                                ?><label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="name <?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');"><?
                                                    ?><span class="val"><?=$ar["VALUE"]?></span><?
                                                    if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                                        ?><span class="role_count">&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)</span><?
                                                    endif;
                                                ?></label><?
                                            ?></div><?
                                        ?></li><?
                                    }
                                    // scroll
                                    if($IS_SCROLABLE) {
                                        ?></div><?
                                    }
                                ?></ul><?
                        }
                    ?></li><?
                }
                ?></ul><?
                
                ?><div class="buttons"><?
                    ?><a rel="nofollow" class="btn1 set_filter" href="<?echo $arResult['FILTER_URL']?>"><?=GetMessage('CT_BCSF_SET_FILTER')?></a><?
                    ?><span class="separator"></span><?
                    ?><a rel="nofollow" class="btn3 del_filter" href="<?=$arResult["SEF_DEL_FILTER_URL"]?>"><?=GetMessage('CT_BCSF_DEL_FILTER')?></a><?
                    ?><input class="nonep" type="submit" id="set_filter" name="set_filter" value="<?=GetMessage('CT_BCSF_SET_FILTER')?>" onclick="smartFilter.reload(this, true)" /><?
                    ?><input class="nonep" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage('CT_BCSF_DEL_FILTER')?>" /><?
                ?></div><?
            ?></div><?
            ?><div class="modef" id="modef" <?if(!isset($arResult['ELEMENT_COUNT'])) echo 'style="display:none"';?>><?
                ?><span class="arrow">&nbsp;</span><?
                ?><span class="data"><?
                    ?><?=GetMessage('CT_BCSF_FILTER_COUNT', array('#ELEMENT_COUNT#' => '<span id="modef_num">'.intval($arResult['ELEMENT_COUNT']).'</span>'));?><?
                    ?><a href="<?echo $arResult['FILTER_URL']?>"><?=GetMessage('CT_BCSF_FILTER_SHOW')?></a><?
                ?></span><?
            ?></div><?
        ?></div><?
    ?></div><?
?></form><?
?><script>
    var smartFilter = new JCSmartFilter('<?=CUtil::JSEscape($arResult['FORM_ACTION'])?>');
</script>