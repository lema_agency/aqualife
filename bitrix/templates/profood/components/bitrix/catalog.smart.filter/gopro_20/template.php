<?
if ( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true )
    die();

$this->setFrameMode(true);

$i = 0;
?>


<div class="filter-block">
    <div class="selects">
        <? foreach ( $arResult['ITEMS'] as $key => $arItem ): ?>
            <? if ( empty($arItem['VALUES']) || isset($arItem['PRICE']) ): ?>
                <? continue ?>
            <? endif; ?>
            <select id="id<?= ++$i ?>">
                <option><?= $arItem['NAME'] ?></option>

                <? foreach ( $arItem["VALUES"] as $val => $ar ): ?>
                    <option name="<?= $ar["CONTROL_NAME"] ?>" VALUE="<?= $ar["CONTROL_NAME"] ?>=Y" ><?= $ar["VALUE"] ?></option>
                <? endforeach; ?>
            </select> 
        <? endforeach; ?>
    </div>
    <div class="actions-wr">
        <a href="?" class="filter btn">Подобрать</a> 
        <a href="#" class="reset btn">Сбросить фильтр</a>
        <div class="summary">
            <b>Всего найдено: </b>
            30 товаров
        </div>
    </div>
</div>   
<script>
    var smartFilter = new JCSmartFilter('<?= CUtil::JSEscape($arResult['FORM_ACTION']) ?>');
</script>