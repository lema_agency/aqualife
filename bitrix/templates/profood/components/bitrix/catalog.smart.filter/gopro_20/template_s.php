<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?><form name="<?=$arResult['FILTER_NAME'].'_form'?>" action="<?=$arResult['FORM_ACTION']?>" method="get" class="smartfilter" onsubmit="return RSGoPro_FilterOnSubmitForm();"><?
    foreach($arResult['HIDDEN'] as $arItem) {
        ?><input <?
            ?>type="hidden" <?
            ?>name="<?=$arItem['CONTROL_NAME']?>" <?
            ?>id="<?=$arItem['CONTROL_ID']?>" <?
            ?>value="<?=$arItem['HTML_VALUE']?>" <?
        ?>/> <?
    }
    ?><div class="around_filtren"><?
        ?><div class="filtren clearfix<?
            if($arParams['FILTER_FIXED']=='Y'):?> filterfixed<?endif;?><?
            if($arParams['FILTER_USE_AJAX']=='Y'):?> ajaxfilter<?endif;?><?
            echo ' '.$arParams['FILTER_DISABLED_PIC_EFFECT'];
            ?>"><?
            ?><div class="title"><?
                ?><a class="shhi" href="#"><span class="show"><?=GetMessage('CT_BCSF_FILTER_TITLE_SHOW')?></span><span class="hide"><?=GetMessage('CT_BCSF_FILTER_TITLE_HIDE')?></span></a><?
                if($arParams['USE_COMPARE']=='Y') {
                    ?><span class="filtercompare"><?=GetMessage('FILTER_COMPARE')?>: <a href="#"></a></span><?
                }
            ?></div>
<?
$i=0;
foreach($arResult['ITEMS'] as $key=>$arItem) {
    
    if( empty($arItem['VALUES']) || isset($arItem['PRICE']) ) {
        continue;
    }
?>
    <select id="id<?=++$i?>">
        <option><?=$arItem['NAME']?></option>
        <?
            foreach($arItem["VALUES"] as $val => $ar){
                   ?>
                    <option name="<?=$ar["CONTROL_NAME"]?>" VALUE="<?=$ar["CONTROL_NAME"]?>=Y" ><?=$ar["VALUE"]?></option>
                   <?
            }
        ?>
    </select> 
<?
}
?> 
<script>
 $('select').on('change', function(){
     $val1=$('#id1').val();
     if($val1.indexOf("Y")!=-1){
        $val1=$('#id1').val()+"&";
     }else{
        $val1=""; 
     }
     
     
     
     $val2=$('#id2').val();
      if($val2.indexOf("Y")!=-1){
        $val2=$('#id2').val()+"&";
     }else{
        $val2=""; 
     }
     
     
     
     $val3=$('#id3').val();
     if($val3.indexOf("Y")!=-1){
        $val3=$('#id3').val()+"&";
     }else{
        $val3=""; 
     }
     
     
     
     $val4=$('#id4').val();
     if($val4.indexOf("Y")!=-1){
        $val4=$('#id4').val();
     }else{
        $val4=""; 
     }
     $('a').attr('href', '?' + $val1  + $val2  + $val3 + $val4+"&set_filter=��������"); 
}).trigger('change');
</script>

<a href="?" id="filter">�����������</a>                            
         
            <div class="body"><?
                ?><ul class="clearfix"><? 
                 foreach($arResult['ITEMS'] as $key=>$arItem) {
                    if( empty($arItem['VALUES']) || isset($arItem['PRICE']) ) {
                        continue;
                    }
                    ?>
                    
                    <li style="list-style-type: none;" class="lvl1<?if($IS_SCROLABLE):?> scrolable<?endif;?><?if($IS_SEARCHABLE):?> searcheble<?endif;?><?if($arItem["DISPLAY_EXPANDED"]!="Y"):?> closed<?endif?>" data-propid="<?=$arItem['ID']?>" data-propcode="<?=$arItem['CODE']?>">
                    <a href="#" class="showchild"><?=$arItem['NAME']?><?if($arItem['FILTER_HINT']<>''):?><span class="hint">?<div><?=$arItem['FILTER_HINT']?></div></span><?endif;?></a><?
                        $arCur = current($arItem["VALUES"]);
                        switch ($arItem["DISPLAY_TYPE"]) {

                            default://CHECKBOXES
                                ?><ul class="property"><?

                                    foreach ($arItem["VALUES"] as $val => $ar) {
                                        $class = "";
                                        if($ar["CHECKED"]) {
                                            $class.= " active";
                                        }
                                        if($ar["DISABLED"]) {
                                            if($class != ' active') {
                                                $class.= " disabled";
                                            }
                                        }
                                        ?><li class="lvl2" style="list-style-type: none;"><?
                                            ?><div class="<?=$class?>"><?
                                                ?><input <?
                                                    ?>type="checkbox" <?
                                                    ?>name="<?=$ar["CONTROL_NAME"]?>" <?
                                                    ?>id="<?=$ar["CONTROL_ID"]?>" <?
                                                    ?>value="<?=$ar["HTML_VALUE"]?>" <?
                                                    ?><? echo $ar["CHECKED"]? 'checked="checked"': '' ?> <?
                                                ?>/>
                                                <label for="<?=$ar["CONTROL_ID"]?>" 
                                                         data-role="label_<?=$ar["CONTROL_ID"]?>" 
                                                         class="name <?=$class?>" 
                                                         onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');">
                                                   
                                                   <span class="val"><?=$ar["VALUE"]?></span>
                                                   
                                                   <?
                                                    if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                                        ?><span class="role_count">&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)</span><?
                                                    endif;
                                                ?>
                                                </label><?
                                            ?></div><?
                                        ?></li><?
                                    }
                                    // scroll
                                    if($IS_SCROLABLE) {
                                        ?></div><?
                                    }
                                ?></ul><?
                        }
                    ?></li><?
                }
                ?></ul><?
                
                ?><div class="buttons"><?
                    ?><a href="<?echo $arResult['FILTER_URL']?>"><?=GetMessage('CT_BCSF_FILTER_SHOW')?></a><?
                    ?><span class="separator"></span><?
                    ?><a rel="nofollow" class="btn3 del_filter" href="<?=$arResult["SEF_DEL_FILTER_URL"]?>"><?=GetMessage('CT_BCSF_DEL_FILTER')?></a><?
                    ?><input class="nonep" type="submit" id="set_filter" name="set_filter" value="<?=GetMessage('CT_BCSF_SET_FILTER')?>" onclick="smartFilter.reload(this, true)" /><?
                    ?><input class="nonep" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage('CT_BCSF_DEL_FILTER')?>" /><?
                ?></div><?
            ?></div><? echo $arResult['FILTER_URL'];
            ?><div class="modef" id="modef" <?if($arResult['ELEMENT_COUNT']==0) echo 'style="display:none"'?>><?
                ?><span class="arrow">&nbsp;</span><?
                ?><span class="data"><?
                    ?><?=GetMessage('CT_BCSF_FILTER_COUNT', array('#ELEMENT_COUNT#' => '<span id="modef_num">'.intval($arResult['ELEMENT_COUNT']).'</span>'));?><?
                    ?><a href="<?echo $arResult['FILTER_URL']?>"><?=GetMessage('CT_BCSF_FILTER_SHOW')?></a><?
                ?></span><?
            ?></div><?
        ?></div><?
    ?></div><?
?></form><?
?><script>
    var smartFilter = new JCSmartFilter('<?=CUtil::JSEscape($arResult['FORM_ACTION'])?>');
</script>