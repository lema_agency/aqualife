<?
if ( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true )
    die();
?>
<?
$menu_has_sub = false;
foreach ( $arResult['ITEMS'] as $key => $arItem ) {
    if ( !(empty($arItem['VALUES']) || isset($arItem['PRICE'])) ) {
        $menu_has_sub = true;
    }
}
?>
<div class="menu">
    <ul>
        <li class='active <?= $menu_has_sub ? "has-sub" : ""; ?>'>
            <a href='#s' class="menu-name">
                <?= $arParams["NAME"] ?>
            </a>
            <? foreach ( $arResult['ITEMS'] as $key => $arItem ) : ?>
                <? if ( empty($arItem['VALUES']) || isset($arItem['PRICE']) ): ?>
                    <? continue; ?>
                <? endif; ?>

                <ul class="submenu">
                    <li class='has-sub'>
                        <a href='#' class="submenu-name"><?= $arItem['NAME'] ?></a>
                        <ul>
                            <? foreach ( $arItem["VALUES"] as $val => $ar ): ?>
                                <li>
                                    <a href='/site_mw/catalog/<?= $arParams["URL"] ?>/?<?= $ar["CONTROL_NAME"] ?>=Y&set_filter=Показать'>
                                        <?= $ar["VALUE"] ?>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </li>
                </ul> 
            <? endforeach; ?>
        </li>
    </ul> 
</div>                            
