<?
if ( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true )
    die();
$this->setFrameMode(true);

$TOP_DEPTH     = $arResult['SECTION']['DEPTH_LEVEL'];
$CURRENT_DEPTH = $TOP_DEPTH;

$strSectionEdit        = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_EDIT');
$strSectionDelete      = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_DELETE');
$arSectionDeleteParams = array('CONFIRM' => GetMessage('RS.ONEAIR.ELEMENT_DELETE_CONFIRM'));

foreach ( $arResult['SECTIONS'] as $key => $arSection ) {
    $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
    if ( $CURRENT_DEPTH < $arSection['DEPTH_LEVEL'] || !$CURRENT_DEPTH ) {
        ?><ul class="<? if ( $CURRENT_DEPTH == $TOP_DEPTH ) { ?>menu-sidebar clearfix<? } ?>"><?
    } elseif ( $CURRENT_DEPTH == $arSection['DEPTH_LEVEL'] ) {
        ?></li><?
    } else {
        while ( $CURRENT_DEPTH > $arSection['DEPTH_LEVEL'] ) {
            ?></li></ul><?
            $CURRENT_DEPTH--;
        }
        ?></li><?
    }
    ?><li id="<?= $this->GetEditAreaId($arSection['ID']); ?>" class="first"><?
    ?><a class="clearfix" href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?
    ?><span class="imya"><?= $arSection['NAME']; ?></span><?
            if ( $arParams['COUNT_ELEMENTS'] ) {
                ?> <span class="count">(<? echo $arSection['ELEMENT_CNT'] ?>)</span><?
            }
            ?></a><?
        $CURRENT_DEPTH = $arSection['DEPTH_LEVEL'];
    }

    while ( $CURRENT_DEPTH > $TOP_DEPTH ) {
        ?></li></ul><?
    $CURRENT_DEPTH--;
}

$this->SetViewTarget('catalog_section_list_descr');
?>
<h1><?= $arResult['SECTION']['NAME'] ?></h1>

<? if ( $arResult['SECTION']['DESCRIPTION'] != '' ): ?>
    <div class="sectinfo">
        <? if ( isset($arResult['SECTION']['PICTURE']['SRC']) ): ?>
            <div class="img clearfix">
                <img src="<?= $arResult['SECTION']['PICTURE']['SRC'] ?>" 
                     alt="<?= $arResult['SECTION']['PICTURE']['ALT'] ?>" 
                     title="<?= $arResult['SECTION']['PICTURE']['TITLE'] ?>" />
            </div>
            <div class="description">
                <?= $arResult['SECTION']['DESCRIPTION'] ?>
            </div>

        <? else: ?>
            <div class="description no_img_descr"><?= $arResult['SECTION']['DESCRIPTION'] ?></div>
        <? endif; ?>
            
        <div class="show-more">
            <div class="t"></div>
        </div>

        <div class="share-friends">
            Рассказать друзьям: 
            <ul>
                <li><a href="#"><img src="/bitrix/templates/profood/img/icon-vk.png"/></a></li>
                <li><a href="#"><img src="/bitrix/templates/profood/img/icon-fb.png"/></a></li>
                <li><a href="#"><img src="/bitrix/templates/profood/img/icon-tw.png"/></a></li>
                <li><a href="#"><img src="/bitrix/templates/profood/img/icon-inst.png"/></a></li>
                <li><a href="#"><img src="/bitrix/templates/profood/img/icon-gl.png"/></a></li>
            </ul>
        </div>
    </div>
<? endif; ?>

<? $this->EndViewTarget(); ?>
