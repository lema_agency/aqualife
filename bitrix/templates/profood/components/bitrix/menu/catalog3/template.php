<?
if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();
$this->setFrameMode(true);
?>
<div class="sidebar">
    <div class="menu-title">Наши товары</div>
    <div class="menu-wr">
        <?
        $APPLICATION->IncludeComponent('bitrix:catalog.smart.filter', 'gopro_202', array('IBLOCK_ID' => 1, 'SECTION_ID' => 39, "NAME" => "Питьевая вода", "URL" => "pitevaya_voda"));
        $APPLICATION->IncludeComponent('bitrix:catalog.smart.filter', 'gopro_202', array('IBLOCK_ID' => 1, 'SECTION_ID' => 2, "NAME" => "Лимонады и напитки", "URL" => "napitki"));
        $APPLICATION->IncludeComponent('bitrix:catalog.smart.filter', 'gopro_202', array('IBLOCK_ID' => 1, 'SECTION_ID' => 53, "NAME" => "Оборудование", "URL" => "napitki"));
        $APPLICATION->IncludeComponent('bitrix:catalog.smart.filter', 'gopro_202', array('IBLOCK_ID' => 1, 'SECTION_ID' => 198, "NAME" => "Сопутствующая прогдукци", "URL" => "napitki"));
        ?>
    </div>
    <div class="menu-title">Мы в соцсетях</div>
    <!-- VK Widget -->
    <div id="vk_groups"></div>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
    <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {mode: 0, width: "230", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '4580A9'}, 52654667);
    </script>
</div>