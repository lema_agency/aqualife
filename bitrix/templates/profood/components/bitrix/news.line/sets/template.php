<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);

?><div class="presscentermainn clearfix"><?
	if( isset($arParams['BLOCK_NAME']) && $arParams['BLOCK_NAME']!='' )
	{
		?><div class="title"><?=$arParams['BLOCK_NAME']?></div><?
	}
	?><div class="in sets-in clearfix"><?
       ?> <div id="slider1"><?
		?><a class="buttons prev" href="#"></a><?
		?><div class="viewport"><?
			?><div class="overview"><?
		$count = count($arResult['ITEMS']);
		foreach($arResult['ITEMS'] as $key => $arItem)
		{
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				
			$t = CIBlockElement::GetProperty(32, $arItem['ID'], array("sort" => "asc"), Array("CODE"=>"PRICE"));	   
				if($price = $t->Fetch())
						$price = $price['VALUE'];
				else
						$price = NULL;
		    $k = CIBlockElement::GetProperty(32, $arItem['ID'], array("sort" => "asc"), Array("CODE"=>"COMPLECT"));	   
				if($complect = $k->Fetch())
						$complect = $complect['VALUE'];
				else
						$complect = NULL;
			?><div class="sets-item<?if($count==($key+1)):?> last<?endif;?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"><?
                               ?><div class="data"><?
					?><a class="name" href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['NAME']?>"><?=$arItem['NAME']?></a><div class='complect'><?= $complect ?></div><?
				?></div><?
				?><div class="img"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" border="0" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" title="<?=$arItem['PREVIEW_PICTURE']['TITLE']?>" /></a><?
				
                         ?></div><?
		?><div class="sets-price"> <?= $price ?> </div><?
								?><a rel="nofollow" class="buy1click detail fancyajax fancybox.ajax sets-buy" href="/site_mw/buy1click/" title="Заказать">Заказать</a><?
			?></div><?
		}
                   ?></div><?
		?></div><?
		?><a class="buttons next" href="#"></a><?
	?></div><?
	?></div><?
?></div>
<script type="text/javascript">
		$(document).ready(function()
		{
			$('#slider1').tinycarousel();
		});
	</script>