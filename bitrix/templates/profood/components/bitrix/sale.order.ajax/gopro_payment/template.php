<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if(!$USER->IsAuthorized())
{
    LocalRedirect(SITE_DIR . 'personal/order/make/');
    exit;
}

$APPLICATION->SetTitle('Подтверждение и оплата');
$APPLICATION->SetPageProperty('title', $APPLICATION->GetTitle(false));

if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
    if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if(strlen($arResult["REDIRECT_URL"]) > 0) {
            $APPLICATION->RestartBuffer();
            ?><script type="text/javascript">window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';</script><?
            die();
        }

    }
}

$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/basket.css");

CJSCore::Init(array('fx','popup','window','ajax'));
$cntBasketItems = CSaleBasket::GetList(
    array(),
    array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "ORDER_ID" => "NULL"
    ),
    array()
);

$basketFolder = '/bitrix/templates/profood/components/bitrix/sale.basket.basket/gopro';

//get user properties
$fieldList = array('PERSONAL_MOBILE', 'PERSONAL_CITY', 'PERSONAL_STREET', 'UF_HOUSE', 'UF_OFFICE', 'PERSONAL_NOTES');
if($t = CUser::GetList($by, $desc, array('ID' => $USER->GetID()), array('SELECT' => $fieldList)))
    $userInfo = $t->Fetch();
else
    $userInfo = array_fill_keys($fieldList, NULL);

/*
$currency = CSaleLang::GetLangCurrency(SITE_ID);

$city = preg_replace_callback('~^(.)(.*?)$~iu', function($m) { return mb_strtoupper($m[1], 'UTF-8') . mb_strtolower($m[2], 'UTF-8'); }, $userInfo['PERSONAL_CITY']);
$cities = CSaleLocation::GetList(array("SORT"=>"ASC", "COUNTRY_NAME_LANG"=>"ASC", "CITY_NAME_LANG"=>"ASC"), array('CITY_NAME' => $city));
$locationId = $cities && ($tmp = $cities->Fetch()) ? $tmp['ID'] : 0;


$arOrder = array(
    "WEIGHT" => "10", // вес заказа в граммах
    "PRICE" => "100", // стоимость заказа в базовой валюте магазина
    "LOCATION_FROM" => COption::GetOptionInt('sale', 'location'), // местоположение магазина
    "LOCATION_TO" => $locationId, // местоположение доставки
);

$deliveryName = $arResult['DELIVERY']['NAME'];

$dbHandler = CSaleDeliveryHandler::GetBySID('simple');
if(!empty($_POST))
{
    $res = CSaleDelivery::GetList(array(), array('NAME' => $deliveryName, 'ACTIVE' => 'Y'));

    //$res = CSaleDeliveryHandler::GetList(array("SORT" => "ASC"), array('NAME' => $delivery['NAME']));
    var_dump($arResult['DELIVERY']['NAME'], $locationId, $res->Fetch(), $dbHandler->Fetch());
    exit;
}

if ($arHandler = $dbHandler->Fetch())
{
    $arProfiles = CSaleDeliveryHandler::GetHandlerCompability($arOrder, $arHandler);
    if (is_array($arProfiles) && count($arProfiles) > 0)
    {
        $arProfiles = array_keys($arProfiles);
        $arReturn = CSaleDeliveryHandler::CalculateFull(
            'simple', // идентификатор службы доставки
            $arProfiles[0], // идентификатор профиля доставки
            $arOrder, // заказ
            $currency // валюта, в которой требуется вернуть стоимость
        );

        if ($arReturn["RESULT"] == "OK")
        {
            ShowNote('Стоимость доставки успешно рассчитана!');
            echo 'Стоимость доставки: '.CurrencyFormat($arReturn["VALUE"], $currency).'<br />';
            if (is_set($arReturn['TRANSIT']) && $arReturn['TRANSIT'] > 0)
            {
                echo 'Длительность доставки: '.$arReturn['TRANSIT'].' дней.<br />';
            }
        }
        else
        {
            ShowError('Не удалось рассчитать стоимость доставки! '.$arResult['ERROR']);
        }
    }
    else
    {
        ShowError('Невозможно доставить заказ!');
    }
}
else
{
    ShowError('Обработчик не найден!');
}
*/
if(isset($_POST['site_offer']))
{
    $arFields = array(
        "LID" => SITE_ID,
        "PERSON_TYPE_ID" => 1,
        "PAYED" => "N",
        "CANCELED" => "N",
        "STATUS_ID" => "N",
        "PRICE" => floatval($arResult['ORDER_PRICE']),
        "CURRENCY" => "RUB",
        "USER_ID" => IntVal($USER->GetID()),
        "PAY_SYSTEM_ID" => intval($_POST['PAY_SYSTEM_ID']),
        "PRICE_DELIVERY" => floatval($arResult['DELIVERY_PRICE']),
        "DELIVERY_ID" => intval($_POST['DELIVERY_ID']),
        "DISCOUNT_VALUE" => 0.0,
        "TAX_VALUE" => 0.0,
        "USER_DESCRIPTION" => $userInfo['PERSONAL_NOTES'],
        'NOTES' => $userInfo['PERSONAL_NOTES'],
    );
    if($orderId = CSaleOrder::Add($arFields))
    {
        $cntBasketItems = CSaleBasket::GetList(
            array(),
            array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
            array()
        );

        //add all basket items
        for($i = $cntBasketItems->SelectedRowsCount(), $basketId = CSaleBasket::GetBasketUserID(); $i; --$i)
            CSaleBasket::OrderBasket($orderId, $basketId, SITE_ID);

        $address  = ($t = trim($userInfo['PERSONAL_STREET'])) ? GetMessage('STREET_SHORT') . $t : '';
        if($t = trim($userInfo['UF_HOUSE']))
            $address .= (empty($address) ? '' : ', ') . GetMessage('HOUSE_SHORT') . $t;
        if($t = trim($userInfo['UF_OFFICE']))
            $address .= (empty($address) ? '' : ', ') . GetMessage('OFFICE_SHORT') . $t;

        //get order properties
        $fieldList = array(
            10 => array('name' => 'Телефон', 'code' => 'F_PHONE', 'value' => $userInfo['PERSONAL_MOBILE']),
            3 => array('name' => 'Адрес (без города)', 'code' => 'ADDRESS', 'value' => $address),
            13 => array('name' => 'Адрес доставки', 'code' => 'F_ADDRESS', 'value' => $userInfo['PERSONAL_CITY']),
            18 => array('name' => 'Комментарий', 'code' => 'COMMENT', 'value' => $userInfo['PERSONAL_NOTES']),
        );

        //add property values to order
        foreach($fieldList as $propId => $prop)
        {
            CSaleOrderPropsValue::Add(array(
                'ORDER_ID' => $orderId,
                'ORDER_PROPS_ID' => $propId,
                'NAME' => $prop['name'],
                'CODE' => $prop['code'],
                'VALUE' => $prop['value'],
            ));
        }

        $arResult["REDIRECT_URL"] = $arParams['PATH_TO_PERSONAL'] . 'order/?ID=' . $orderId;

        LocalRedirect($arResult['REDIRECT_URL']);
    }
}

//if basket is empty then redirect or show error. Otherwise show this content page
if(!$cntBasketItems->SelectedRowsCount()) :
    if(isset($arParams['DISABLE_BASKET_REDIRECT']) && $arParams['DISABLE_BASKET_REDIRECT'] == 'N')
        LocalRedirect($arParams['PATH_TO_BASKET']);
    else
        echo ShowError(GetMessage('BASKET_NO_ITEMS'));
else :
?>
<div class="basket_head_links">
    <a href="<?=SITE_DIR;?>personal/cart/" class="active">Корзина</a>
    <a href="<?=SITE_DIR;?>personal/order/make/" class="active">Оформление заказа</a>
    <a href="<?=SITE_DIR;?>personal/order/payment/" class="active">Подтверждение и оплата</a>
</div>
<div id="order_form_div" class="order_form_div someform orderforma order-checkout"><?
    ?><NOSCRIPT><div class="errortext"><?=GetMessage("SOA_NO_JS")?></div></NOSCRIPT><?


    if (!function_exists("getColumnName")) {
        function getColumnName($arHeader) {
            return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
        }
    }

    if (!function_exists("cmpBySort")) {
        function cmpBySort($array1, $array2) {
            if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
                return -1;

            if ($array1["SORT"] > $array2["SORT"])
                return 1;

            if ($array1["SORT"] < $array2["SORT"])
                return -1;

            if ($array1["SORT"] == $array2["SORT"])
                return 0;
        }
    }

    ?><div class="bx_order_make">
        <h4 class="subtitle"><?=GetMessage('USER_INFO')?></h4>
            <table id="seller_info">
                <tr>
                    <td>
                        <div class="input">
                            <label for="seller_name"><?=GetMessage('SELLER_NAME');?></label>
                            <input id="seller_name" type="text" name="new_user[seller_name]" value="<?=$USER->GetFullName();?>" readonly>
                        </div>
                    </td>
                    <td>
                        <div class="input">
                            <label for="seller_phone"><?=GetMessage('SELLER_PHONE');?></label>
                            <input type="tel" id="seller_phone" name="new_user[seller_phone]" value="<?=$userInfo['PERSONAL_MOBILE'];?>" readonly>
                        </div>
                    </td>
                    <td>
                        <div class="input">
                            <label for="seller_email"><?=GetMessage('SELLER_EMAIL');?></label>
                            <input type="text" id="seller_email" name="new_user[seller_email]" value="<?=$USER->GetEmail();?>" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input">
                            <label for="seller_address"><?=GetMessage('SELLER_ADDRESS');?></label>
                            <input type="text" id="seller_address" name="new_user[seller_address]" value="<?=$userInfo['PERSONAL_CITY'];?>" readonly>
                        </div>
                    </td>
                    <td rowspan="2">
                        <div class="input">
                            <label for="seller_street"><?=GetMessage('SELLER_STREET');?></label>
                            <textarea id="seller_street" name="new_user[seller_street]" readonly><?=$userInfo['PERSONAL_STREET'];?></textarea>
                        </div>
                    </td>
                    <td rowspan="2">
                        <div class="input">
                            <label for="seller_address_comment"><?=GetMessage('SELLER_ADDRESS_COMMENT');?></label>
                            <textarea id="seller_address_comment" name="new_user[seller_address_comment]" readonly><?=$userInfo['PERSONAL_NOTES'];?></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input small">
                            <label for="seller_house"><?=GetMessage('SELLER_HOUSE');?></label>
                            <input type="text" id="seller_house" name="new_user[seller_house]" value="<?=$userInfo['UF_HOUSE'];?>" readonly>
                        </div>
                        <div class="input small">
                            <label for="seller_office"><?=GetMessage('SELLER_OFFICE');?></label>
                            <input type="text" id="seller_office" name="new_user[seller_office]" value="<?=$userInfo['UF_OFFICE'];?>" readonly>
                        </div>
                    </td>
                </tr>
            </table>
        <form method="post" action="" name="order_auth_form">
            <?=bitrix_sessid_post()?>
            <div class="ordertable t1<?php if($isNewUser) : ?> hidden<?php endif; ?>" id="phone_auth">

            </div>
            <div class="clear"></div>
            <?

            if(!$isNewUser)
            {
                include $_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php";
                include $_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php";
                include $_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php";
            }
            ?>
            <div class="clear"></div>
            <div class="checkbox offer">
                <input type="checkbox" name="site_offer" id="site_offer">
                <label for="site_offer">
                    <?=GetMessage('SITE_OFFER');?>
                    <a href="#" onclick="return false;"><?=GetMessage('SITE_OFFER_NAME');?></a>
                </label>
            </div>
            <?if($_POST["is_ajax_post"] != "Y") {
            ?>
    </div>
<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
<input type="hidden" name="profile_change" id="profile_change" value="N">
<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
<input type="hidden" name="json" value="Y">
    <div class="btns clearfix">
        <div class="bx_ordercart_order_pay_center">
            <input class="btn btn1 confirm inactive" type="submit" id="confirm_order" name="BasketOrder" value="<?=GetMessage('SOA_TEMPL_BUTTON')?>" />
        </div>
        <?
        if($arParams["DELIVERY_NO_AJAX"] == "N") {
            ?>
            <div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
            <?
        }
        } else {
            ?>
            <script type="text/javascript">
                top.BX('confirmorder').value = 'Y';
                top.BX('profile_change').value = 'N';
                window.parent.checkLocation();
            </script>
            <?
            die();
        }
        ?>

        </form>
        </div><?
    if(CSaleLocation::isLocationProEnabled()) {
        ?><div style="display: none">
        <?// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.steps",
            ".default",
            array(
            ),
            false
        );?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.search",
            "",
            array(
            ),
            false
        );?>
        </div><?
    }
    endif;
    ?></div>