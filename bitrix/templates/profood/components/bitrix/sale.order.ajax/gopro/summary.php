<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$bDefaultColumns = $arResult['GRID']['DEFAULT_COLUMNS'];
$colspan = ($bDefaultColumns) ? count($arResult['GRID']['HEADERS']) : count($arResult['GRID']['HEADERS']) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column

?>

<div class="basket">
	<h4 class="subtitle"><?=GetMessage('SALE_PRODUCTS_SUMMARY');?></h4>

	<table class="items">
		<thead>
			<tr>
				<th class="xxx">№</th>
				<th class="xxx"><?=GetMessage('BASKET_PHOTO')?></th>
				<th class="xxx"><?=GetMessage('BASKET_NAME')?></th>
				<th class="xxx"><?=GetMessage('BASKET_COUNT')?></th>
				<th class="xxx"><?=GetMessage('BASKET_COST')?></th>
				<th class="xxx"><?=GetMessage('BASKET_TOTAL_COST')?></th>
				<? /*<th class="xxx"><?=GetMessage('SALE_DELETE')?></th> */ ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach($arResult['GRID']['ROWS'] as $arData) : ?>
			<? $arItem = $arData['data']; ?>
				<tr>
					<?php /*<td class="checkbox"><input type="checkbox" name="DELETE_<?=$arItem['ID']?>" id="DELETE_<?=$arItem['ID']?>" value="Y"><label for="DELETE_<?=$arItem['ID']?>"></label></td>*/ ?>
					<td class="xxx"><?=++$cnt;?></td>
					<td class="image"><div>
							<?php
								// image
								if(!empty($arItem['DETAIL_PICTURE_SRC']))
										$url = $arItem['DETAIL_PICTURE_SRC'];
								elseif(!empty($arItem['PREVIEW_PICTURE_SRC']))
										$url = $arItem['PREVIEW_PICTURE_SRC'];
								else
										$url = $arResult['NO_PHOTO']['src'];

								if(strlen($arItem['DETAIL_PAGE_URL'])>0) : ?>
									<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
								<?php endif; ?>
								<img src="<?=$url?>" alt="" />
								<?php if(strlen($arItem['DETAIL_PAGE_URL'])>0) : ?></a><?php endif; ?></div>
					</td>
					<td class="product_name">
							<?php if(!empty($arItem['DETAIL_PAGE_URL'])) : ?><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?php endif;?>
								<?=$arItem['NAME']?>
							<?php if(!empty($arItem['DETAIL_PAGE_URL'])) : ?></a><?php endif; ?>
					</td>
					<td class="tc">
							<?php if($arShowColumns['quantity']>0 && $mode=='') : ?>
									<?=$arItem['MEASURE_TEXT']?>
							<?php else : ?>
									<?=$arItem['QUANTITY']?>
									<span class="js-measurename"><?=$arItem['MEASURE_TEXT']?></span>
							<?php endif; ?>
					</td>
					<td class="tc">
							<span class="lppadding nowrap">
									<?=$arItem['PRICE_FORMATED']?>
							</span>
					</td>
					<td class="tc">
							<span class="lppadding nowrap">
									<?=$arItem['SUM']?>
							</span>
					</td>
					<? /*
					<td class="tc">
							<a class="delete" href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" title="<?=GetMessage('SALE_DELETE')?>"><i class="icon pngicons"></i></a>
					</td>
					*/ ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div class="total">
		<span class="totaltext">
			<span class="name"><?=GetMessage('BASKET_SALE_SUM')?>:</span>
			<span class="take_allSum_FORMATED"><?=$arResult['ORDER_TOTAL_PRICE_FORMATED']?></span>
		</span>
		<div class="print_order_list">
			<img src="<?=SITE_TEMPLATE_PATH;?>/img/print-icon.gif" alt="">
			<a href="#"><?=GetMessage('PRINT_ORDER_LIST');?></a>
		</div>
		<div class="clear"></div>
		<iframe id="print_frame" style="display:none"></iframe>
		<script>
				$(function() {
						var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
						var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
						$('.print_order_list a').on('click', function() {
							doc.getElementsByTagName('body')[0].innerHTML = '<table>' + $('table.items').html() + '</table>'
							win.print()
							return false;
						})
				})
		</script>
	</div>
</div>
<?php return ; ?>
...............
		<? $cnt = 0;
		foreach($arResult['GRID']['ROWS'] as $k => $arData)
		{
			?>
			<tr>
				<td class="xxx">
					<?=++$cnt;?>
				</td>
				<? if($bShowNameWithPicture) : ?>
					<td class="image">
						<div class="img">
							<?php if(strlen($arData['data']['PREVIEW_PICTURE_SRC'])>0)
								$url = $arData['data']['PREVIEW_PICTURE_SRC'];
							elseif (strlen($arData['data']['DETAIL_PICTURE_SRC'])>0)
								$url = $arData['data']['DETAIL_PICTURE_SRC'];
							else
								$url = $arResult['NO_PHOTO']['src'];
							
							if(strlen($arData['data']['DETAIL_PAGE_URL'])>0) : ?>
								<a href="<?=$arData['data']['DETAIL_PAGE_URL'] ?>">
							<?php endif; ?>
							
							<img src="<?=$url?>" alt="" title="" />
							<?php if(strlen($arData['data']['DETAIL_PAGE_URL'])>0) : ?>
								</a>
							<?php endif; ?>
						</div>
					</td>
				<? endif;
			// prelimenary check for images to count column width
			foreach($arResult['GRID']['HEADERS'] as $id => $arColumn)
			{
				$arItem = (isset($arData['columns'][$arColumn['id']])) ? $arData['columns'] : $arData['data'];
				
				if(is_array($arItem[$arColumn['id']]))
				{
					foreach($arItem[$arColumn['id']] as $arValues)
						if($arValues['type']=='image')
							$imgCount++;
				}
			}

			foreach($arResult['GRID']['HEADERS'] as $id => $arColumn)
			{
				$class = ($arColumn['id']=='PRICE_FORMATED')?'price':'';
				if(in_array($arColumn['id'], array('PROPS','TYPE','NOTES', 'DISCOUNT_PRICE_PERCENT_FORMATED'))) // some values are not shown in columns in this template
					continue;

				if($arColumn['id']=='PREVIEW_PICTURE' && $bShowNameWithPicture)
					continue;

				$arItem = (isset($arData['columns'][$arColumn['id']]))?$arData['columns']:$arData['data'];

				if($arColumn['id']=='NAME') : ?>
					<td class="product_name">
						<span class="bx_ordercart_itemtitle">
							<?php if(strlen($arItem['DETAIL_PAGE_URL'])>0) : ?>
								<a href="<?=$arItem['DETAIL_PAGE_URL'] ?>">
							<?php endif; ?>
							<?=$arItem['NAME']?>
							<?php if(strlen($arItem['DETAIL_PAGE_URL'])>0) : ?>
								</a>
							<?php endif; ?>
						</span>
					<div class="bx_ordercart_itemart">
						<?php if($bPropsColumn) : ?>
							<?php foreach($arItem['PROPS'] as $val) : ?>
								<?=$val['NAME']?>: <span><?=$val['VALUE']?><span><br/>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
					</td>
				<?php elseif ($arColumn['id']=='PRICE_FORMATED') : ?>
					<td class="price right">
						<div class="current_price"><?=$arItem['PRICE_FORMATED']?> </div>
							<div class="old_price right">
								<?php if(doubleval($arItem['DISCOUNT_PRICE'])>0) : ?>
									<?=SaleFormatCurrency($arItem['PRICE'] + $arItem['DISCOUNT_PRICE'], $arItem['CURRENCY'])?>
									<?php $bUseDiscount = true; ?>
								<?php endif; ?>
							</div>
							<?php if($bPriceType && strlen($arItem['NOTES'])>0) : ?>
								<div style="text-align: left">
									<div class="type_price"><?=GetMessage('SALE_TYPE')?></div>
									<div class="type_price_value"><?=$arItem['NOTES']?></div>
								</div>
							<?php endif; ?>
						</div>
					</td>
					<?php /*elseif ($arColumn['id']=='DISCOUNT') : ?>
						<td class="custom right">
							<?=$arItem['DISCOUNT_PRICE_PERCENT_FORMATED']?>
						</td>
					<?php */ elseif ($arColumn['id']=='DETAIL_PICTURE' && $bPreviewPicture) : ?>
						<td>
							<div class="img">
								<?php
								$url = '';
								if($arColumn['id']=='DETAIL_PICTURE' && strlen($arData['data']['DETAIL_PICTURE_SRC'])>0)
									$url = $arData['data']['DETAIL_PICTURE_SRC'];
								
								if($url=='')
									$url = $templateFolder.'/images/no_photo.png';
								if(strlen($arData['data']['DETAIL_PAGE_URL'])>0): ?>
									<a href="<?=$arData['data']['DETAIL_PAGE_URL']?>">
								<?php endif; ?>
								<img src="<?=$url?>" alt="" title="" />
								<?php if(strlen($arData['data']['DETAIL_PAGE_URL'])>0) : ?>
									</a>
								<?php endif; ?>
							</div>
						</td>
					<?php elseif (in_array($arColumn['id'], array('QUANTITY','WEIGHT_FORMATED','DISCOUNT_PRICE_PERCENT_FORMATED','SUM'))) : ?>
						<?php if($arColumn['id'] == "SUM") : ?>
							<td class="tc">
								<?=$arItem[$arColumn['id']]?>
							</td>
						<?php else : ?>
							<td class="tc">
								<?=$arItem[$arColumn['id']]?>
							</td>
						<?php endif; ?>
					<?php else : ?>
						<?php if(is_array($arItem[$arColumn['id']]))
						{
							foreach($arItem[$arColumn['id']] as $arValues)
							{
								if($arValues['type']=='image')
									$columnStyle = 'width:20%';
							}
							?>
							<td class="tc" style="<?=$columnStyle?>">
								<?
								foreach($arItem[$arColumn['id']] as $arValues)
								{
									if($arValues['type']=='image')
									{
										?>
										<div class="img">
											<img src="<?=$arValues['value']?>" alt="" title="" />
										</div>
									<?
									}
									else
									{ // not image
										?>
										<?=$arValues['value']?><br/>
										<?
									}
								}
								?>
							</td>
							<?
						}
						else
						{ // not array, but simple value
							?>
							<td class="tc" style="<?=$columnStyle?>">
								<?=$arItem[$arColumn['id']];?>
							</td>
							<?
						}
					endif;
				}
			?>
			</tr>
		<? } ?>
	</tbody>
</table>

<div class="order_pay">
	<div class="right">
		<table class="bx_ordercart_order_sum">
			<tbody>
			
				<?php if( doubleval($arResult['ORDER_WEIGHT'])>0 ) : ?>
					<tr>
						<td class="custom_t1" colspan="<?=$colspan?>" class="itog"><?=GetMessage('SOA_TEMPL_SUM_WEIGHT_SUM')?></td>
						<td class="custom_t2 price"><?=$arResult['ORDER_WEIGHT_FORMATED']?></td>
					</tr>
				<?php endif; ?>
				
				<?php if( $arResult['ORDER_PRICE_FORMATED']!=$arResult['ORDER_TOTAL_PRICE_FORMATED'] ) : ?>
					<tr>
						<td class="custom_t1" colspan="<?=$colspan?>" class="itog"><?=GetMessage('SOA_TEMPL_SUM_SUMMARY')?></td>
						<td class="custom_t2 price"><?=$arResult['ORDER_PRICE_FORMATED']?></td>
					</tr>
				<?php endif; ?>
				
				<?php if(doubleval($arResult['DISCOUNT_PRICE'])>0) : ?>
					<tr>
						<td class="custom_t1" colspan="<?=$colspan?>" class="itog"><?=GetMessage('SOA_TEMPL_SUM_DISCOUNT')?><?if(strLen($arResult['DISCOUNT_PERCENT_FORMATED'])>0):?> (<?=$arResult['DISCOUNT_PERCENT_FORMATED'];?>)<?endif;?>:</td>
						<td class="custom_t2 price"><?=$arResult['DISCOUNT_PRICE_FORMATED']?></td>
					</tr>
				<?php endif; ?>
				
				<?php if(!empty($arResult['TAX_LIST'])) : ?>
					<?php foreach($arResult['TAX_LIST'] as $val) : ?>
						<tr>
							<td class="custom_t1" colspan="<?=$colspan?>" class="itog"><?=$val['NAME']?> <?=$val['VALUE_FORMATED']?>:</td>
							<td class="custom_t2 price"><?=$val['VALUE_MONEY_FORMATED']?></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
				
				<?php if(doubleval($arResult["DELIVERY_PRICE"])>0) : ?>
					<tr>
						<td class="custom_t1" colspan="<?=$colspan?>" class="itog"><?=GetMessage('SOA_TEMPL_SUM_DELIVERY')?></td>
						<td class="custom_t2 price"><?=$arResult['DELIVERY_PRICE_FORMATED']?></td>
					</tr>
				<?php endif; ?>
				
				<?php if(strlen($arResult['PAYED_FROM_ACCOUNT_FORMATED'])>0) : ?>
					<tr>
						<td class="custom_t1" colspan="<?=$colspan?>" class="itog"><?=GetMessage('SOA_TEMPL_SUM_PAYED')?></td>
						<td class="custom_t2 price"><?=$arResult['PAYED_FROM_ACCOUNT_FORMATED']?></td>
					</tr>
				<?php endif; ?>
				
				<?php if($bUseDiscount) : ?>
					<tr>
						<td class="custom_t1 fwb" colspan="<?=$colspan?>" class="itog"><?=GetMessage('SOA_TEMPL_SUM_IT')?></td>
						<td class="custom_t2 fwb price"><?=$arResult['ORDER_TOTAL_PRICE_FORMATED']?></td>
					</tr>
					<tr>
						<td class="custom_t1" colspan="<?=$colspan?>"></td>
						<td class="custom_t2" style="text-decoration:line-through; color:#828282;"><?=$arResult['PRICE_WITHOUT_DISCOUNT']?></td>
					</tr>
				<?php else : ?>
					<tr>
						<td class="custom_t1 fwb" colspan="<?=$colspan?>" class="itog"><?=GetMessage('SOA_TEMPL_SUM_IT')?></td>
						<td class="custom_t2 fwb price"><?=$arResult['ORDER_TOTAL_PRICE_FORMATED']?></td>
					</tr>
				<?php endif; ?>
				
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
<div class="section">
	<h4><?=GetMessage('SOA_TEMPL_SUM_COMMENTS')?></h4>
	<div class="body">
		<textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" style="max-width:100%;min-height:120px"><?=$arResult['USER_VALS']['ORDER_DESCRIPTION']?></textarea>
	</div>
	<input type="hidden" name="" value="" />
</div>
<p>
	<div> </div>
</p>