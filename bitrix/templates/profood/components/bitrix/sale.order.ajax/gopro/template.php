<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($USER->IsAuthorized() && isset($_POST['old_client']))
{
    LocalRedirect(SITE_DIR . 'personal/order/payment/');
    exit;
}

//register and/or auth user
if(!empty($_POST['new_user']) && is_array($_POST['new_user']))
{
    $values = array_map('trim', $_POST['new_user']);
    $user = new CUser;

    $values['seller_phone'] = preg_replace('~\\D+~', '', $values['seller_phone']);
    if(strlen($values['seller_phone']) === 11)
        $values['seller_phone'] = substr($values['seller_phone'], 1);

    $arFields = Array(
        "NAME"              => $values['seller_name'],
        "LAST_NAME"         => "",
        "EMAIL"             => $values['seller_email'],
        "LOGIN"             => $values['seller_phone'],
        "PERSONAL_MOBILE"   => $values['seller_phone'],
        "PERSONAL_CITY"	    => $values['seller_address'],
        "PERSONAL_STREET"   => $values['seller_street'],
        "UF_HOUSE"          => $values['seller_house'],
        "UF_OFFICE"          => $values['seller_office'],
        "LID"               => "ru",
        "ACTIVE"            => "Y",
        "GROUP_ID"          => array(3,4),
        "PASSWORD"          => "123456",
        "CONFIRM_PASSWORD"  => "123456",
        "PERSONAL_NOTES"    => $values['seller_address_comment'],
    );

    $t = CUser::GetList($by, $order, array('LOGIN' => $values['seller_login']));

    if($u = $t->Fetch())
        $userId = $user->Update($u['ID'], $arFields) ? $u['ID'] : 0;
    else
        $userId = $user->Add($arFields);

    if (intval($userId) > 0)
    {
        $USER->Authorize($userId);
        LocalRedirect(SITE_DIR . 'personal/order/payment/');
    }
    else
        echo $user->LAST_ERROR;
}


$isNewUser = !$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N";

//get or set default user properties
$fieldList = array('PERSONAL_MOBILE', 'PERSONAL_CITY', 'PERSONAL_STREET', 'UF_HOUSE', 'UF_OFFICE', 'PERSONAL_NOTES');
if($isNewUser)
    $userInfo = array_fill_keys($fieldList, NULL);
else
{
    $t = CUser::GetList($by, $desc, array('ID' => $USER->GetID()), array('SELECT' => $fieldList));
    $userInfo = $t->Fetch();
}

$APPLICATION->SetTitle('Оформление заказа');
$APPLICATION->SetPageProperty('title', $APPLICATION->GetTitle(false));


if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
    if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if(strlen($arResult["REDIRECT_URL"]) > 0) {
            $APPLICATION->RestartBuffer();
            ?><script type="text/javascript">window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';</script><?
            die();
        }

    }
}

$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/basket.css");

CJSCore::Init(array('fx','popup','window','ajax'));
$cntBasketItems = CSaleBasket::GetList(
    array(),
    array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "ORDER_ID" => "NULL"
    ),
    array()
);

$basketFolder = '/bitrix/templates/profood/components/bitrix/sale.basket.basket/gopro';

//if basket is empty then redirect or show error. Otherwise show this content page
if(!$cntBasketItems->SelectedRowsCount()) :
    if(isset($arParams['DISABLE_BASKET_REDIRECT']) && $arParams['DISABLE_BASKET_REDIRECT'] == 'N')
        LocalRedirect($arParams['PATH_TO_BASKET']);
    else
        echo ShowError(GetMessage('BASKET_NO_ITEMS'));
else :
?>
<div id="userInfo">
    <? $APPLICATION->IncludeComponent(
        "bxmod:auth.dialog",
        "phone_auth",
        array(
            "SUCCESS_RELOAD_TIME" => "5",
            "COMPONENT_TEMPLATE" => "phone_auth"
        ),
        false
    ); ?>
</div>
<div class="basket_head_links">
    <a href="<?=SITE_DIR;?>personal/cart/" class="active">Корзина</a>
    <a href="<?=SITE_DIR;?>personal/order/make/" class="active">Оформление заказа</a>
    <a href="<?=SITE_DIR;?>personal/order/payment/">Подтверждение и оплата</a>
</div>
<div id="order_form_div" class="order_form_div someform orderforma order-checkout">
    <NOSCRIPT><div class="errortext"><?=GetMessage("SOA_NO_JS")?></div></NOSCRIPT>

    <form method="post" action="" name="order_auth_form">
    <div class="bx_order_make">
        <hr class="clear">
        <div class="checkboxes">
            <div class="checkbox">
                <input type="checkbox" name="new_client" id="new_client"<?php if($isNewUser) : ?> checked<?php endif; ?>> <label for="new_client"><?=GetMessage('NEW_CLIENT');?></label>
            </div>
            <div class="checkbox">
                <input type="checkbox" name="old_client" id="old_client"<?php if(!$isNewUser) : ?> checked<?php endif; ?>> <label for="old_client"><?=GetMessage('OLD_CLIENT');?></label>
            </div>
        </div>
        <hr class="clear">
            <?=bitrix_sessid_post()?>
            <table id="seller_info"<?php if(!$isNewUser) : ?> class="hidden"<?php endif; ?>>
                <tr>
                    <td>
                        <div class="input">
                            <label for="seller_name"><?=GetMessage('SELLER_NAME');?></label>
                            <input id="seller_name" type="text" name="new_user[seller_name]" value="<?=$USER->GetFullName();?>">
                        </div>
                    </td>
                    <td>
                        <div class="input">
                            <label for="seller_phone"><?=GetMessage('SELLER_PHONE');?></label>
                            <input type="tel" id="seller_phone" name="new_user[seller_phone]" value="<?=$userInfo['PERSONAL_MOBILE'];?>">
                        </div>
                    </td>
                    <td>
                        <div class="input">
                            <label for="seller_email"><?=GetMessage('SELLER_EMAIL');?></label>
                            <input type="text" id="seller_email" name="new_user[seller_email]" value="<?=$USER->GetEmail();?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input">
                            <label for="seller_address"><?=GetMessage('SELLER_ADDRESS');?></label>
                            <input type="text" id="seller_address" name="new_user[seller_address]" value="<?=$userInfo['PERSONAL_CITY'];?>">
                        </div>
                    </td>
                    <td rowspan="2">
                        <div class="input">
                            <label for="seller_street"><?=GetMessage('SELLER_STREET');?></label>
                            <textarea id="seller_street" name="new_user[seller_street]"><?=$userInfo['PERSONAL_STREET'];?></textarea>
                        </div>
                    </td>
                    <td rowspan="2">
                        <div class="input">
                            <label for="seller_address_comment"><?=GetMessage('SELLER_ADDRESS_COMMENT');?></label>
                            <textarea id="seller_address_comment" name="new_user[seller_address_comment]"><?=$userInfo['PERSONAL_NOTES'];?></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input small">
                            <label for="seller_house"><?=GetMessage('SELLER_HOUSE');?></label>
                            <input type="text" id="seller_house" name="new_user[seller_house]" value="<?=$userInfo['UF_HOUSE'];?>">
                        </div>
                        <div class="input small">
                            <label for="seller_office"><?=GetMessage('SELLER_OFFICE');?></label>
                            <input type="text" id="seller_office" name="new_user[seller_office]" value="<?=$userInfo['UF_OFFICE'];?>">
                        </div>
                    </td>
                </tr>
            </table>
            <?=bitrix_sessid_post()?>
            <div class="ordertable t1<?php if($isNewUser) : ?> hidden<?php endif; ?>" id="phone_auth">

            </div>
            <div class="clear"></div>
    </div>
    <div class="btns clearfix">
        <div class="bx_ordercart_order_pay_center">
            <input class="btn btn1 confirm" id="confirm_order" type="submit" name="BasketOrder" value="<?=GetMessage('BASKET_SALE_CONFIRM')?>" />
            <input class="btn btn1 fast buy1click detail fancyajax fancybox.ajax" href="/site_mw/buy1click/" type="submit" name="BasketFastOrder" value="<?=GetMessage('BASKET_FAST_SALE_ORDER')?>" onclick="return false;" />
        </div>
    </div>
    </form>
<?php endif;