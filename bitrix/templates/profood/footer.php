<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
IncludeTemplateLangFile(__FILE__);
				?></div><?
			?></div><?
		?></div><!-- /content --><?
	?></div><!-- /body --><?
	// scripts
	?><script type="text/javascript">RSGoPro_SetSet();</script><?


?><div id="footer" class="footer"><!-- footer --><?
		?><div class="centering"><?
			?><div class="centeringin line1 clearfix"><?
				?><div class="block one"><?
					?><div class="contacts clearfix"><?
	$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"infootercatalog", 
	array(
		"ROOT_MENU_TYPE" => "footercatalog",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"BLOCK_TITLE" => "Товары",
		"LVL1_COUNT" => "6",
		"LVL2_COUNT" => "0",
		"ELLIPSIS_NAMES" => "Y",
		"COMPONENT_TEMPLATE" => "infootercatalog"
	),
	false
);
					?></div><?
				?></div><?
				?><div class="block two"><?
				$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"infootercatalog", 
	array(
		"ROOT_MENU_TYPE" => "footer",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "4",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"BLOCK_TITLE" => "",
		"LVL1_COUNT" => "200",
		"LVL2_COUNT" => "20",
		"ELLIPSIS_NAMES" => "Y",
		"COMPONENT_TEMPLATE" => "infootercatalog",
		"RSGOPRO_CATALOG_PATH" => "/catalog/",
		"RSGOPRO_MAX_ITEM" => "9",
		"RSGOPRO_IS_MAIN" => "N",
		"RSGOPRO_PROPCODE_ELEMENT_IN_MENU" => "",
		"IBLOCK_ID" => "",
		"PRICE_CODE" => "",
		"PRICE_VAT_INCLUDE" => "N",
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CONVERT_CURRENCY" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"MENU_THEME" => "site"
	),
	false
);
				?></div><?
				?><div class="block three"><?
					$APPLICATION->IncludeComponent("bitrix:menu", "infooter", array(
	"ROOT_MENU_TYPE" => "footer",
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "N",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => "",
		"BLOCK_TITLE" => "",
		"COMPONENT_TEMPLATE" => "infooter",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);
				?></div><?
				?><div class="block four"><?
				?></div><?
		    	?><div class="sitecopy"><?
						$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH."/include_areas/sitecopy.php",
							Array(),
							Array("MODE"=>"html")
						);
					?></div><?
			?></div><?
		?></div><?

		?><div class="line2"><?
			?><div class="centering"><?
				?><div class="centeringin clearfix"><?
					?><div class="sitecopy"><?
						$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH."/include_areas/footer_sitecopy.php",
							Array(),
							Array("MODE"=>"html")
						);
					?></div><?
		            		?><div class="subscribe"><?
						$APPLICATION->IncludeComponent(
							"bitrix:subscribe.form",
							"footer",
							array(
								"USE_PERSONALIZATION" => "Y",
								"SHOW_HIDDEN" => "N",
								"PAGE" => "/site_mw/personal/subscribe/",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "36000000",
							),
							false
						);
					?></div><?
		           ?><div class="btn-zhaloba"><a class="fancyajax fancybox.ajax recall" href="/site_mw/recall/" title="<?=GetMessage('RSGOPRO.REV')?>"><?=GetMessage('RSGOPRO.REV')?></a></div><?
				?></div><?
			?></div><?
		?></div><?
	?></div><!-- /footer --><?
	?><div id="fixedcomparelist"><?
		$APPLICATION->IncludeComponent(
			"bitrix:catalog.compare.list",
			"session",
			array(
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => "1",
				"NAME" => "CATALOG_COMPARE_LIST",
			),
			false
		);
	?></div><?
	$APPLICATION->IncludeComponent(
		"redsign:easycart",
		"gopro",
		array(
			"USE_VIEWED" => "Y",
			"USE_COMPARE" => "Y",
			"USE_BASKET" => "Y",
			"USE_FAVORITE" => "Y",
			"VIEWED_COUNT" => "10",
			"FAVORITE_COUNT" => "10",
			"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
			"TEMPLATE_THEME" => "lime",
			"Z_INDEX" => "991",
			"MAX_WIDTH" => "1240",
			"USE_ONLINE_CONSUL" => "Y",
			"ONLINE_CONSUL_LINK" => "#",
			"INCLUDE_JQUERY" => "N",
			"INCLUDE_JQUERY_COOKIE" => "N",
			"INCLUDE_JQUERY_STICKY" => "N",
			"ADD_BODY_PADDING" => "Y",
			"ON_UNIVERSAL_AJAX_HANDLER" => "Y",
			"UNIVERSAL_AJAX_FINDER" => "action=ADD2BASKET",
			"COMPARE_IBLOCK_TYPE" => "catalog",
			"COMPARE_IBLOCK_ID" => "1",
			"COMPARE_RESULT_PATH" => "/site_mw/catalog/compare/",
			"UNIVERSAL_AJAX_FINDER_COMPARE" => "action=ADD_TO_COMPARE_LIST",
			"UNIVERSAL_AJAX_FINDER_BASKET" => "action=ADD2BASKET",
			"UNIVERSAL_AJAX_FINDER_FAVORITE" => "action=add2favorite",
			"UNIVERSAL_AJAX_FINDER_COMPARE_ADD" => "action=ADD_TO_COMPARE_LIST",
			"UNIVERSAL_AJAX_FINDER_COMPARE_REMOVE" => "action=DELETE_FROM_COMPARE_LIST"
		),
		false
	);
?></body><?
?></html>